//
//  ViewController.m
//  Hello World
//
//  Created by Deepthi Kaligi on 19/07/2017.
//  Copyright © 2017 bananaapps. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor greenColor]];
    [self.view setBackgroundColor:[UIColor yellowColor]];
    
    UIButton *gmailButton = [UIButton buttonWithType:UIButtonTypeSystem];
    gmailButton.frame =  CGRectMake(self.view.frame.size.width/2-100, 20, 200, 40);
    [gmailButton setTitle:@"gmail button" forState:UIControlStateNormal];
    [self.view addSubview:gmailButton];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
