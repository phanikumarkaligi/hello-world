//
//  AppDelegate.h
//  Hello World
//
//  Created by Deepthi Kaligi on 19/07/2017.
//  Copyright © 2017 bananaapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

